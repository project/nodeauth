README

Node Authorization allows users to add simple HTTP authorization to individual
nodes. This is not a replacement for the user and content access system, just a
lightweight alternative to create simple access restrictions on per-node basis.
Note that HTTP authorization is not a totally secure system, particularly on
non-HTTPS sites. If you require robust security to protect sensitive node
content, please use Drupal's more established content access system.
